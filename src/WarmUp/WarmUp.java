package WarmUp;

import java.util.Arrays;

public class WarmUp {
    public static void main(String[] args) {
        int[] one = {1, 1, 1, 1, 1}; //Массив
        int time = 3; // Время по условию
        Arrays.sort(one);
        System.out.println(Arrays.toString(one)); //Преобразует в строку

        int[] expensive = countOf(one, time);

        int summa = 0;
        for (int i = 0; i < expensive.length; i++) {
            summa += expensive[i];
        }
        System.out.println(summa);
    }

    private static int[] countOf(int[] array, int res) {
        int[] expensive = new int[res];
        if (res > array.length) {
            res = array.length;
        }
        for (int i = 0; i < res; i++) {
            expensive[i] = array[array.length - i - 1];
        }
        return expensive;
    }
}